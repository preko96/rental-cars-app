import styled from 'react-emotion'
import mq from '../../util/mq'

export default styled('div')`
	.root {
		height: 100vh;
	}
	.welcome {
		${mq({ display: ['none', 'block'] })}
		${mq({ marginTop: [0, 50] })}
	}
	.header {
		padding-left: 0;
		padding-right: 0;
		justify-content: flex-end;
		display: flex;
		align-items: flex-end;
		background-color: #0b76c5;
	}
	.searchBox {
		${mq({ marginTop: [0, 50] })}
	}
`