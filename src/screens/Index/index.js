import React from 'react'
import { Layout, Row, Col } from 'antd'
import MainHeader from '../../containers/MainHeader/'
import SearchBox from '../../containers/SearchBox/'
import Welcome from '../../components/Welcome/'
import Styled from './styled'

const { Content } = Layout

//using a Styled component as root (react-emotion)

const Index = () =>
	<Styled>
		<Layout className='root'>
			<Layout>
				<MainHeader/>			
				<Content>
					<Welcome/>
					<Row>
						<Col sm={24} md={12}>
							<SearchBox />
						</Col>
					</Row>
				</Content>
			</Layout>
		</Layout>
	</Styled>

export default Index