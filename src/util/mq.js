import facepaint from 'facepaint'

/*
	query selector helpers (use the function with react-emotion)
*/

const breakpoints = [576, 768, 992, 1200]

export default facepaint(
	breakpoints.map(
		bp => `@media (min-width: ${bp}px)`
	)
)