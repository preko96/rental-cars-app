import { all } from 'redux-saga/effects'
import searchBox from './searchBox/saga'

export default function* rootSaga() {
	yield all([
		searchBox()
	])
}
