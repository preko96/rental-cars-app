import {
	ON_CHANGE_INPUT,
	ON_FOCUS_INPUT,
	ON_BLUR_INPUT,
	START_FETCH,
	END_FETCH_SUCCESS,
	END_FETCH_FAIL
} from './constants'

const initState = {
	input: '',
	focused: false,
	pending: false,
	results: [],
	error: null,
}

export default (state=initState, action={}) => {
	switch(action.type) {
	case ON_CHANGE_INPUT:
		return { 
			...state, 
			input: action.payload,
			pending: false,
			results: state.input.length < 2 ? 
				[] : 
				state.results 
		}
	case ON_FOCUS_INPUT:
		return { ...state, focused: true }
	case ON_BLUR_INPUT: 
		return { ...state, focused: false }
	case START_FETCH:
		return { ...state, pending: true }
	case END_FETCH_SUCCESS:
		return { ...state, pending: false, results: action.payload }
	case END_FETCH_FAIL:
		return { ...state, pending: false, error: action.payload }
	default: 
		return state
	}
}