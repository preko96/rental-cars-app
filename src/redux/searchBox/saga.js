import { all, call, fork, takeLatest, put } from 'redux-saga/effects'
import axios from 'axios'
import { startFetch, endFetchSuccess, endFetchFail } from './actions'
import { ON_CHANGE_INPUT } from './constants'

const delay = (ms) => new Promise(res => setTimeout(res, ms))

function* handleResults(results) {
	if(!results || results.numFound === 0)
		return yield put(endFetchFail('No match found'))

	const handled = results.docs.map(unit=>({
		name: unit.name,
		region: unit.region,
		country: unit.country,
	}))

	yield put(endFetchSuccess(handled))
}

function* getResultsWorker(action) {
	//yield is similar to await
	yield delay(300)
	const number = 6
	const term = action.payload
	if(term.length > 1) {
		//put equals -> dispatch
		yield put(startFetch())
		const endpoint = `${process.env.REACT_APP_API_URL}FTSAutocomplete.do?solrIndex=fts_en&solrRows=${number}&solrTerm=${term}`
		try {
			const response = yield call(axios.get, endpoint)
			const results = response.data.results
			//fork -> start async task
			yield fork(handleResults, results)
		} catch(error) {
			yield put(endFetchFail(error))
		}
	}
}

//takeLatest -> for easy debouncing
export default function* rootSaga() {
	yield all([
		takeLatest(ON_CHANGE_INPUT, getResultsWorker),
	])
}