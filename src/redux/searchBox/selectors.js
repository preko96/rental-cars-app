import { createSelector } from 'reselect'

//using reselect so I won't recalculate the selectors in each mounted connected component.

export const selectInput = state => state.searchBox.input
export const selectResults = state => state.searchBox.results
export const selectIsFocused = state => state.searchBox.focused

export const resultsSelector = createSelector(
	selectResults,
	results=>results
)

export const shouldDisplayResultsSelector = createSelector(
	selectInput,
	resultsSelector,
	selectIsFocused,
	(input, results, isFocused) =>
		input.length > 1 &&
		results.length > 0 &&
		isFocused
)