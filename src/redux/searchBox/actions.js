import {
	ON_CHANGE_INPUT,
	ON_FOCUS_INPUT,
	ON_BLUR_INPUT,
	START_FETCH,
	END_FETCH_SUCCESS,
	END_FETCH_FAIL
} from './constants'

export const onChangeInput = text => ({
	type: ON_CHANGE_INPUT,
	payload: text
})

export const onFocusInput = () => ({
	type: ON_FOCUS_INPUT
})

export const onBlurInput = () => ({
	type: ON_BLUR_INPUT
})

export const startFetch = () => ({
	type: START_FETCH,
})

export const endFetchSuccess = items => ({
	type: END_FETCH_SUCCESS,
	payload: items
})

export const endFetchFail = error => ({
	type: END_FETCH_FAIL,
	payload: error
})