import { user, saga } from '../../util/createReduxModule'

const moduleUser = name => user('SearchBox', name)
const moduleSaga = name => saga('SearchBox', name)

export const ON_CHANGE_INPUT = moduleUser('ON_CHANGE_INPUT')
export const ON_FOCUS_INPUT = moduleUser('ON_FOCUS_INPUT')
export const ON_BLUR_INPUT = moduleUser('ON_BLUR_INPUT')
export const START_FETCH = moduleSaga('START_FETCH')
export const END_FETCH_SUCCESS = moduleSaga('END_FETCH_SUCCESS')
export const END_FETCH_FAIL = moduleSaga('END_FETCH_FAIL')