import React from 'react'
import PropTypes from 'prop-types'
import { Icon } from 'antd'
import Styled from './styled'

const Item = ({ name, title }) =>
	<Styled>
		<Icon type={name} className='icon'/>
		<div className='title'>{title}</div>
	</Styled>

Item.propTypes = {
	name: PropTypes.string,
	title: PropTypes.string
}

export default Item