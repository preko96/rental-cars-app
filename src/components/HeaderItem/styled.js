import styled from 'react-emotion'

export default styled('div')`
	color: white;
	min-width: 64px;
	justify-content: center;
	height: 100%;
	display: flex;
	padding: 0 20px;
	align-items: center;
	&:hover {
		background-color: rgba(0, 0, 0, .1);
		cursor: pointer;
	}
	.icon {
		font-size: 20px;
	}
`