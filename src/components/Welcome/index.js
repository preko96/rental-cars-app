import React from 'react'
import H from '../../components/H'

const Welcome = () =>
	<div className='welcome'>
		<H size='1' align='center'>Car Hire – Search, Compare & Save</H>
		<H size='2' align='center'>Compare 900 companies at over 53,000 locations. Best price guaranteed</H>
	</div>

export default Welcome