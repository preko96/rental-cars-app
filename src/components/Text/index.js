import React from 'react'
import PropTypes from 'prop-types'

const Text = ({ children, style, ...rest }) =>
	<div style={{...style}} {...rest}>
		{ children }
	</div> 

Text.propTypes = {
	children: PropTypes.node,
	style: PropTypes.object
}

export default Text