import React from 'react'
import PropTypes from 'prop-types'

const H = ({ size, style, align, weight, color, children, ...rest }) => {
	const HeaderText = `h${size}`
	return (
		<HeaderText style={{ color: color, marginBottom: 0, textAlign: align, fontWeight: weight, ...style}} {...rest}>
			{children}
		</HeaderText>
	)
}

H.defaultProps = {
	size: 3,
	align: 'initial',
	color: 'black',
}

H.propTypes = {
	size: PropTypes.oneOfType([
		PropTypes.number,
		PropTypes.string
	]),
	style: PropTypes.object,
	children: PropTypes.node,
	align: PropTypes.string,
	weight: PropTypes.oneOfType([
		PropTypes.number,
		PropTypes.string
	]),
	color: PropTypes.string,
}

export default H