import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import 'antd/dist/antd.css'
import store from './redux/store'
import Index from './screens/Index'
import dotenv from 'dotenv'

dotenv.config()

const EnhancedApp = () => 
	<Provider store={store}>
		<Index/>
	</Provider>

ReactDOM.render(<EnhancedApp />, document.getElementById('root'))
