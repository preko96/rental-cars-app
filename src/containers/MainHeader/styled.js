import styled from 'react-emotion'
import mq from '../../util/mq'

export default styled('div')`
	.icon {
		${mq({ marginRight: [0, 10] })}
	}
	.title {
		${mq({ display: ['none', 'initial'] })}
	}
`