import React from 'react'
import { Layout } from 'antd'
import HeaderItem from '../../components/HeaderItem/'
import Styled from './styled'

const { Header } = Layout

const MainHeader = () => 
	<Styled>
		<Header className='header'>
			<HeaderItem name='book' title='Manage booking'/>
			<HeaderItem name='user' title='Sign In'/>
		</Header>
	</Styled>

export default MainHeader