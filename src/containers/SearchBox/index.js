import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {  Row, Col, Input, Button, Icon } from 'antd'
import ResultList from '../ResultList/'
import H from '../../components/H/'
import ShowAt from '../../components/ShowAt/'
import Flex from '../../components/Flex/'
import Text from '../../components/Text'
import { shouldDisplayResultsSelector } from '../../redux/searchBox/selectors'
import { onChangeInput, onFocusInput, onBlurInput } from '../../redux/searchBox/actions'

class SearchBox extends React.Component {
	
	//this way we won't receive false on shallow compare or on shouldComponentUpdate
	//(no inline arrow functions)
	onChange = e => this.props.onChangeInput(e.target.value)

	render() {
		const { shouldDisplay, isPending, onFocusInput, onBlurInput } = this.props
		return(
			<Flex padder padding='5%' backgroundColor='#f3ce56' className='searchBox'>
				<H size='1' weight='700' style={{ marginBottom: 10 }}>Lets find your ideal car!</H>
				<Text style={{ fontSize: 18 }}>Pickup Location</Text>
				<Input
					suffix={<Icon type={isPending ? 'loading' : 'search'} style={{ color: isPending ? '#1890ff' : '#d9d9d9' }} />}
					onFocus={onFocusInput}
					onBlur={onBlurInput} 
					onChange={this.onChange} 
					placeholder='city, airport, station, region, district...' 
					style={{ height: 40 }}
				/>
				<ShowAt visible={shouldDisplay}>
					<ResultList/>
				</ShowAt>
				<Row style={{ marginTop: '5%' }}>
					<Col sm={24} md={12}/>
					<Col sm={24} md={12}>
						<Button type="primary" block style={{
							height: 64,
						}}>
							<H size='1' align='center' color='white'>Search</H>
						</Button>
					</Col>
				</Row>
			</Flex>
		)
	}
}

SearchBox.propTypes = {
	isPending: PropTypes.bool.isRequired,
	shouldDisplay: PropTypes.bool.isRequired,
	onChangeInput: PropTypes.func.isRequired,
	onFocusInput: PropTypes.func.isRequired,
	onBlurInput: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
	isPending: state.searchBox.pending,
	shouldDisplay: shouldDisplayResultsSelector(state)
})

const mapDispatchToProps = dispatch => ({
	onChangeInput: text => dispatch(onChangeInput(text)),
	onFocusInput: () => dispatch(onFocusInput()),
	onBlurInput: () => dispatch(onBlurInput())
})

export default connect(
	mapStateToProps, 
	mapDispatchToProps
)(SearchBox)