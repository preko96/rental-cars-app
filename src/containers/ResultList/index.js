import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Flex from '../../components/Flex'
import { resultsSelector } from '../../redux/searchBox/selectors'


const ResultList = ({ items }) => 
	<Flex padder backgroundColor='white' style={{ 
		border: '1px solid #d9d9d9', 
		borderRadius: 4, 
		overflow: 'hidden' 
	}}>
		{
			items.map(item=> {
				const value = `${item.name} ${item.region} ${item.country}`
				return <div key={value}>{value}</div>
			}
			)
		}
	</Flex>

ResultList.propTypes = {
	items: PropTypes.array.isRequired
}

const mapStateToProps = state => ({
	items: resultsSelector(state)
})

export default connect(mapStateToProps)(ResultList)